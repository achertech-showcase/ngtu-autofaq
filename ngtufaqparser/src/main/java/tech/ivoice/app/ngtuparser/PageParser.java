package tech.ivoice.app.ngtuparser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class PageParser {
    public Map<String, String> parsePage(int page) {
        Map<String, String> pageQa = new HashMap<>();
        Document doc;
        try {
            doc = Jsoup.connect(format("https://www.nstu.ru/enrollee/answers?page_num=%d", page)).get();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        Elements qaBlocks = doc.select(".bottomLine");
        for (Element qaBlock : qaBlocks) {
            try {
                Elements paragraphs = qaBlock.select("p");
                Iterator<Element> iterator = paragraphs.iterator();
                iterator.next();
                String question = fix(iterator.next().text());
                String answer = fix(iterator.next().text());
                if (!question.equals("") && !answer.equals("")) {
                    pageQa.put(question, answer);
                }
            } catch (Exception e) {
                try {
                    Elements paragraphs = qaBlock.select("p");
                    List<Node> nodes = paragraphs.first().childNodes();
                    String question = fix(((Element) nodes.get(9)).select("i").text());
                    String answer = fix(((TextNode) nodes.get(17)).text());
                    if (!question.equals("") && !answer.equals("")) {
                        pageQa.put(question, answer);
                    }
                } catch (Exception e2) {
                    // ignore
                }

            }

        }
        return pageQa;

    }

    private static String fix(String text) {
        return text.trim();
    }
}
