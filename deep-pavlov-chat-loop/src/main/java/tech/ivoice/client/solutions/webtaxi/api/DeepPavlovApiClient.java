package tech.ivoice.client.solutions.webtaxi.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.List;

public class DeepPavlovApiClient {

    private static DeepPavlovApiClient instance;
    private Gson GSON;

    private CloseableHttpClient httpClient;

    private DeepPavlovApiClient() {
        this.httpClient = HttpClientBuilder.create().build();
        this.GSON = new GsonBuilder().create();
    }

    public static DeepPavlovApiClient getInstance() {
        if (instance == null) {
            instance = new DeepPavlovApiClient();
        }
        return instance;
    }

    public DeepPavlovResponse execute(DeepPavlovRequest request) {
        List<List<String>> deepPavlovResponse;
        try {
            String jsonRequest = GSON.toJson(request);
            StringEntity requestEntity = new StringEntity(jsonRequest, ContentType.APPLICATION_JSON);

            HttpPost postMethod = new HttpPost("http://94.130.53.140:8305/model");
            postMethod.setEntity(requestEntity);

            HttpResponse response = httpClient.execute(postMethod);

            boolean success = response.getStatusLine().getStatusCode() == 200;
            if (success) {
                String jsonResult = new BasicResponseHandler().handleResponse(response);
                //noinspection unchecked
                deepPavlovResponse = (List<List<String>>) GSON.fromJson(jsonResult, List.class);
                return new DeepPavlovResponse(deepPavlovResponse.get(0).get(0));
            } else {
                return new DeepPavlovResponse("извините, я вас не поняла");
            }

        } catch (IOException e) {
            return new DeepPavlovResponse("извините, я вас не поняла");
        }
    }

}
