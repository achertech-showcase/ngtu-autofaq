package tech.ivoice.client.solutions.webtaxi.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeepPavlovResponse implements ApiResponse {

    @SerializedName("response")
    @Expose
    private String response;

    public DeepPavlovResponse() {
    }

    public DeepPavlovResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeepPavlovResponse that = (DeepPavlovResponse) o;

        return response != null ? response.equals(that.response) : that.response == null;
    }

    @Override
    public int hashCode() {
        return response != null ? response.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DeepPavlovResponse{" +
                "response='" + response + '\'' +
                '}';
    }
}
