package tech.ivoice.client.solutions.webtaxi;

import akka.actor.ActorRef;
import tech.ivoice.client.solutions.webtaxi.api.ApiActor;
import tech.ivoice.client.solutions.webtaxi.api.ApiRequest;
import tech.ivoice.platform.extension.sdk.fsm_support.FsmScenario;
import tech.ivoice.platform.extension.sdk.fsm_support.Platform;
import tech.ivoice.platform.extension.sdk.fsm_support.components.wait_init.WaitInitComponent;
import tech.ivoice.platform.fsm.OutState;
import tech.ivoice.platform.sdk.annotations.Scenario;

import java.util.function.Consumer;

import static tech.ivoice.platform.extension.sdk.fsm_support.Tools.link;

@Scenario("deeppavlov_loop2")
public class DeepPavlovLoopScenario extends FsmScenario {

    private ActorRef api = getContext().actorOf(ApiActor.props());

    @Override
    public void initialize(Platform platform, OutState<WaitInitComponent> init) {
        Consumer<ApiRequest> apiConsumer = (message) -> api.tell(message, self());
        ChatComponent chatComponent = new ChatComponent(platform, apiConsumer);
        link(init, chatComponent.entry);
    }
}
