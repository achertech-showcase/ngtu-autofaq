package tech.ivoice.client.solutions.webtaxi.api;

import akka.actor.Props;
import akka.actor.UntypedActor;

public class ApiActor extends UntypedActor {
    private DeepPavlovApiClient apiClient = DeepPavlovApiClient.getInstance();

    public static Props props() {
        return new Props(ApiActor::new);
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof DeepPavlovRequest) {
            DeepPavlovResponse response = apiClient.execute((DeepPavlovRequest) message);
            sender().tell(response, self());
        }
    }
}
