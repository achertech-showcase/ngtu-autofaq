package tech.ivoice.client.solutions.webtaxi.api;

import java.util.Collections;
import java.util.List;

public class DeepPavlovRequest implements ApiRequest {

    private List<String> context;

    public DeepPavlovRequest() {
    }

    public DeepPavlovRequest(String text) {
        this.context = Collections.singletonList(text);
    }

    public List<String> getContext() {
        return context;
    }

    public void setContext(List<String> context) {
        this.context = context;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeepPavlovRequest that = (DeepPavlovRequest) o;

        return context != null ? context.equals(that.context) : that.context == null;
    }

    @Override
    public int hashCode() {
        return context != null ? context.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DeepPavlovRequest{" +
                "context=" + context +
                '}';
    }
}
