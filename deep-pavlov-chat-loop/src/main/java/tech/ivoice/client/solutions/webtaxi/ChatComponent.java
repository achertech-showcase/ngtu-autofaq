package tech.ivoice.client.solutions.webtaxi;

import tech.ivoice.client.solutions.webtaxi.api.ApiRequest;
import tech.ivoice.client.solutions.webtaxi.api.DeepPavlovRequest;
import tech.ivoice.client.solutions.webtaxi.api.DeepPavlovResponse;
import tech.ivoice.platform.extension.sdk.components.request_text.RequestTextComponent;
import tech.ivoice.platform.extension.sdk.fsm_support.Platform;
import tech.ivoice.platform.fsm.DataState;
import tech.ivoice.platform.fsm.FsmContext;
import tech.ivoice.platform.fsm.OutState;
import tech.ivoice.platform.fsm.State;
import tech.ivoice.platform.sdk.messages.Session;
import tech.ivoice.platform.sdk.messages.common.PlaySource;
import tech.ivoice.platform.sdk.messages.session.Play;
import tech.ivoice.platform.sdk.messages.session.PlayCollectSpeech;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static tech.ivoice.platform.extension.sdk.fsm_support.Tools.link;
import static tech.ivoice.platform.extension.sdk.fsm_support.Tools.submitLink;
import static tech.ivoice.platform.sdk.messages.Messages.playCollectSpeech;

public class ChatComponent {
    private Logger logger = Logger.getLogger(ChatComponent.class.getName());
    private static final String SILENCE = "";

    private final FsmContext<ChatComponent> context = FsmContext.of(this);
    private Platform platform;
    private Consumer<ApiRequest> api;

    private RequestTextComponent requestQuestion;

    private DeepPavlovResponse response;
    private static final String COLLECTED_TEXT = "собранный текст";

    ChatComponent(Platform platform, Consumer<ApiRequest> apiConsumer) {
        this.platform = platform;
        this.api = apiConsumer;
        this.requestQuestion = new RequestTextComponent(platform, this::requestQuestionPCS);

        link(startRequestQuestion, requestQuestion.start);
        submitLink(requestQuestion.success, callApi, platform, COLLECTED_TEXT);
        submitLink(requestQuestion.fail, callApi, platform, COLLECTED_TEXT, SILENCE);
    }

    public State<ChatComponent> entry = context.state("entry")
            .onEnter((message, state, states, actions) -> actions.redirect(states.startRequestQuestion))
            .build();

    private DataState<ChatComponent> onPlayFinished(Object message, DataState<ChatComponent> state, ChatComponent states) {
        if (message instanceof Play.Success) {
            return states.startRequestQuestion;
        } else if (message instanceof Play.Fail) {
            return states.finish.withData(message);
        } else return state;
    }

    private OutState<ChatComponent> startRequestQuestion = context.outState("startRequestQuestion");

    @SuppressWarnings("FieldCanBeLocal")
    private State<ChatComponent> callApi = context.state("callApi")
            .onEnter((message, prevState, states) -> {
                String recognition = message.toString();
                api.accept(new DeepPavlovRequest(recognition));
            })
            .onProcess((message, currentState, states) -> {
                logger.log(Level.INFO, message.toString());
                if (message instanceof DeepPavlovResponse) {
                    this.response = (DeepPavlovResponse) message;
                    logger.info(this.response.toString());
                    return states.playApiResponse;
                } else
                    return currentState;
            })
            .build();

    private State<ChatComponent> playApiResponse = context.state("playApiResponse")
            .onEnter((message, state, states) -> platform.accept(playResponse(response.getResponse())))
            .onProcess(this::onPlayFinished)
            .build();

    private OutState<ChatComponent> finish = context.outState("finish", false);

    private PlayCollectSpeech requestQuestionPCS() {
        return playCollectSpeech(PlaySource.urls("http://audios.ivoice.online/deep_pavlov_chat_loop/canIHelp.wav"))
                .setDelayBeforeCollect(1)
                .setTimeout(10)
                .setSilenceAfterSpeechTimeout(3)
                .setWaitingInputTimeout(3)
                .setInput(PlayCollectSpeech.InputType.SPEECH)
                .build();
    }

    private Play playResponse(String response) {
        return new Play(new Session(), PlaySource.speechSynthesis(response));
    }
}
