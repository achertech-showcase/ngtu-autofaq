package tech.ivoice.client.solutions.webtaxi.api;

import org.junit.jupiter.api.Test;

import static java.lang.System.out;

class DeepPavlovApiClientTest {

    DeepPavlovApiClient client = DeepPavlovApiClient.getInstance();

    @Test
    public void test() {
        DeepPavlovRequest request = new DeepPavlovRequest("как пройти в библиотеку");
        DeepPavlovResponse response = client.execute(request);
        out.println(response);
    }

    @Test
    public void test2() {
        DeepPavlovRequest request = new DeepPavlovRequest("");
        DeepPavlovResponse response = client.execute(request);
        out.println(response);
    }

}